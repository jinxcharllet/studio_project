#pragma once
#include <iostream>
#include <string>



struct monster {
	int monsterhealth;
	int monsteratk;
	int monsterdefence;
	int monsterspeed;
	int monsterevasion;
	int expGiven;
	int monsterWeak;
	//Monster Weaknesses//
	// 4 = NIL //
	// 1 = Slashing //
	// 2 = Piercing //
	// 3 = Bludgeoning //
};

struct player {
	int maxhealth;
	int health;
	int maxatk;                 //UnmodifiedAtk//
	int reducedATK;             //Max ATK for user in Vorheez battle//
	int atk;
	int defence;
	int speed;
	int evasion;
	int defencestatus;
	int blockDamageBonus;
	int exp;
	int level;
};

struct inventory {
	int lesserPotion;
	int regularPotion;
	int greaterPotion;
};

struct armours {
	int modifiedAtk;
	int modifiedDefence;
	int modifiedEvasion;
	int modifiedSpeed;
	int thornMail;
};

struct weapons {
	int weaponDamage;
	int weaponType;
	int weaponAttackBonus;
	// Weapon Types//
	// 0 = NIL //
	// 1 = Slashing //
	// 2 = Piercing //
	// 3 = Bludgeoning //
};


void monsterLevelSelect();      //Select Monster Level//
void levelOneMonster();         //Level 1 Monster Data//
void levelTwoMonster();         //Level 2 Monster Data//
void levelThreeMonster();       //Level 3 Monster Data//
void levelFourMonster();        //Level 4 Monster Data//
void levelFiveMonster();        //Level 5 Monster Data//
void bossSelect();              //Select Boss To Fight//
void weaponSelect();            //Weapon Tier Selection//
void shitTier();                //Shit Tier Weapon List//
void okTier();                  //OK Tier Weapon List//
void goodTier();                //Good Tier Weapon List//
void greatTier();               //Great Tier Weapon List//
void godTier();                 //God Tier Weapon List//
void armourSelect();            //Armour Tier Select//
void shitArmour();              //Shit Tier Armour List//
void okArmour();                //Ok Tier Armour List//
void goodArmour();              //Good Tier Armour List//
void greatArmour();             //Great Tier Armour List//
void godArmour();               //God Tier Armour List//
void potionMenu();              //Use Potion Menu//
void monsterAttack();           //If Monster Attack//
void monsterAttackBlock();      //Monster Attack + User Block//
void playerAttack();            //If Player Attack//
void attackPatterns();          //Boss Attack Patterns//
void kingSlime();               //King Slime Battle//
void undeadGiant();             //Giant Undead Battle//
void jasonVorheez();            //Jason Vorheez Battle//
void illuminati();              //Illuminati Battle//
void satan();                   //Satan Battle//
void jesus();                   //JEsus Battle//
void checkLevel();              //Level/Exp Detector//
void timeDelay();               //Creates Time Delay between actions//
void selectChoice();            //gets player input for main battle menu//
void potionChoice();            //gets player input for potionmenu//
void itemChoice();              //gets player input for item pickup//
void itemPickup();              //Words for item pickup selection//
void deathOfEnemy();            //If Enemy Dies//