// This is the main file for the game logic and function
//
//
#include "game.h"
#include "Framework\console.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
#include <iostream>
#include <Windows.h>
#include "WindowsExercution.h"

using namespace std;

double g_dBounceTime;
double g_dElapsedTime;
double g_dDeltaTime;
bool   g_abKeyPressed[K_COUNT];
bool   PlayerTurn;
bool   boss;
bool   InBattle;
int potionUse;
int choice;
int maplevel;
// 1st roll  = hp, 2nd roll = attack, 3rd roll = defences
int enemyStatsArray[3][15] =
{
	//slime,GiantRat,bat ,skeleton,zombie,undeadhound ,vampire,frankenstein,mummy ,necromancer,witch,occultist ,unicorn,giantAnt,centaur
	{ 10,7,5,12,20,15,25,30,20,45,25,35,40,55,45 },
	{ 3,4,5,8,5,6,9,7,12,8,15,12,20,11,15 },
	{ 4,3,2,3,8,6,9,12,7,15,7,12,12,20,15 },

};
int mapArray[50][200];
int titleArray[12][95];
int artArray[40][40];
int enemyNo = 0;//0 = TITLE,Slime 1, Giant Rats 2, Bats 3, King Slime 4,Skeleton 5,Zombie 6,Undead Hounds 7,Undead Giant 8, Vampire 9, Frankenstein 10, Mummy 11, Jason 12, Necromancer 13, Witch 14, Occultist 15, Illuminati 16, Unicorn 17, Giant Ants 18, Centaur 19, satan 20
int keys = 0;
int option;
int sanity;
int map = 1;
bool monsterAlive;
string monName;
// map size reference
//int mapOne[32][108];
//int mapTwo[27][113];
//int mapthree[22][64];
//int mapfour[19][62];//to change
//int mapFive[33][121];

// Game specific variables here
SGameChar   g_sChar;
EGAMESTATES g_eGameState = S_LOADING;

static playerStats user;
static monsterStats enemyMon;

// Console object
//80,25
Console g_Console(130, 50, "SP1 Framework");

//--------------------------------------------------------------
// Purpose  : Initialisation function
//            Initialize variables, allocate memory, load data from file, etc. 
//            This is called once before entering into your main loop
// Input    : void
// Output   : void
//--------------------------------------------------------------
void init(void)
{
	// Set precision for floating point output
	g_dElapsedTime = 0.0;
	g_dBounceTime = 0.0;
	sanity = 200;

	// sets the initial state for the game
	g_eGameState = S_LOADING;

	user.health = 100;
	user.attack = 5;
	user.defence = 3;
	user.potions = 3;
	user.exp = 0;
	user.level = 1;

	enemyMon.monhealth = 0;

	monsterAlive = false;

	ambientMusic();

	g_sChar.m_cLocation.X = g_Console.getConsoleSize().X / 2;
	g_sChar.m_cLocation.Y = g_Console.getConsoleSize().Y / 2;
	g_sChar.m_bActive = true;
	// sets the width, height and the font name to use in the console
	g_Console.setConsoleFont(0, 16, L"Consolas");
}

//--------------------------------------------------------------
// Purpose  : Reset before exiting the program
//            Do your clean up of memory here
//            This is called once just before the game exits
// Input    : Void
// Output   : void
//--------------------------------------------------------------
void shutdown(void)
{
	// Reset to white text on black background
	colour(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);

	g_Console.clearBuffer();
}

//--------------------------------------------------------------
// Purpose  : Getting all the key press states
//            This function checks if any key had been pressed since the last time we checked
//            If a key is pressed, the value for that particular key will be true
//
//            Add more keys to the enum in game.h if you need to detect more keys
//            To get other VK key defines, right click on the VK define (e.g. VK_UP) and choose "Go To Definition" 
//            For Alphanumeric keys, the values are their ascii values (uppercase).
// Input    : Void
// Output   : void
//--------------------------------------------------------------
void getInput(void)
{
	g_abKeyPressed[K_UP] = isKeyPressed(VK_UP);
	g_abKeyPressed[K_DOWN] = isKeyPressed(VK_DOWN);
	g_abKeyPressed[K_LEFT] = isKeyPressed(VK_LEFT);
	g_abKeyPressed[K_RIGHT] = isKeyPressed(VK_RIGHT);
	g_abKeyPressed[K_SPACE] = isKeyPressed(VK_SPACE);
	g_abKeyPressed[K_ESCAPE] = isKeyPressed(VK_ESCAPE);
	g_abKeyPressed[K_RETURN] = isKeyPressed(VK_RETURN);
	g_abKeyPressed[K_W] = isKeyPressed('W');
	g_abKeyPressed[K_S] = isKeyPressed('S');
	g_abKeyPressed[K_A] = isKeyPressed('A');
	g_abKeyPressed[K_D] = isKeyPressed('D');
	g_abKeyPressed[K_1] = isKeyPressed('1');
	g_abKeyPressed[K_2] = isKeyPressed('2');
	g_abKeyPressed[K_3] = isKeyPressed('3');
	g_abKeyPressed[K_4] = isKeyPressed('4');
}

//--------------------------------------------------------------
// Purpose  : Update function
//            This is the update function
//            double dt - This is the amount of time in seconds since the previous call was made
//
//            Game logic should be done here.
//            Such as collision checks, determining the position of your game characters, status updates, etc
//            If there are any calls to write to the console here, then you are doing it wrong.
//
//            If your game has multiple states, you should determine the current state, and call the relevant function here.
//
// Input    : dt = deltatime
// Output   : void
//--------------------------------------------------------------
void update(double dt)
{
	// get the delta time
	g_dElapsedTime += dt;
	g_dDeltaTime = dt;

	switch (g_eGameState)
	{

	case S_LOADING: LOADING(); // game logic for the splash screen
		break;
	case S_MAINMENU: renderMainMenu();
		break;
	case S_INSTRUCTIONMENU: renderInstructionsMenu();
		break;
	case S_GAME: gameplay(); // gameplay logic when we are in the game
		break;
	case S_LEVELS: renderLevels();
		break;
	case S_QUITMENU: renderQuitMenu();
		break;
	case S_DEATH: DeathControls();
		break;
	case S_WIN:WinControls();
		break;
	
	}
}
//--------------------------------------------------------------
// Purpose  : Render function is to update the console screen
//            At this point, you should know exactly what to draw onto the screen.
//            Just draw it!
//            To get an idea of the values for colours, look at console.h and the URL listed there
// Input    : void
// Output   : void
//--------------------------------------------------------------
void render()
{
	clearScreen();      // clears the current screen and draw from scratch 
	switch (g_eGameState)
	{
	case S_LOADING: renderLOADING();
		break;
	case S_MAINMENU: renderMainMenu();
		break;
	case S_GAME: renderGame();
		break;
	case S_QUITMENU: renderQuitMenu();
		break;
	case S_INSTRUCTIONMENU: renderInstructionsMenu();
		break;
	case S_LEVELS: renderLevels();
		break;
	case S_STORY: renderstory();
		break;
	case S_DEATH: renderplayerDied();
		break;
	case S_WIN: WinScreen();
		break;
	}
	renderFramerate();  // renders debug information, frame rate, elapsed time, etc
	renderToScreen();   // dump the contents of the buffer to the screen, one frame worth of game
}

void LOADING()    // waits for time to pass in splash screen
{
	if (g_dElapsedTime > 2.0)  //wait for 3 seconds to switch to game mode, else do nothing
		g_eGameState = S_MAINMENU;
}

void renderLOADING()
{
	COORD c = g_Console.getConsoleSize();
	ASCIIRead();
	renderASCII();

	c.Y = 20;
	c.X = 34;
	g_Console.writeToBuffer(c, "LEVI IS WOKE UP IN A STRANGE PLACE ITS DARK HE MUST GET OUT ", getColour(option, 0));
}

void clearScreen()
{
	// Clears the buffer with this colour attribute
	g_Console.clearBuffer(0x00);
}

void gameplay()            // gameplay logic
{
	processUserInput(); // checks if you should change states or do something else with the game, e.g. pause, exit
	moveCharacter();    // moves the character, collision detection, physics, etc
						// sound can be played here too.
}

void processUserInput()
{
	if (g_abKeyPressed[K_ESCAPE])
		g_eGameState = S_QUITMENU;
}

void moveCharacter()
{
	if (user.exp >= 100)
	{
		levelup();
	}
	if (InBattle == false)
	{
		int ghostplayerX = g_sChar.m_cLocation.X;
		int ghostplayerY = g_sChar.m_cLocation.Y;
		bool bSomethingHappened = false;
		if (g_dBounceTime > g_dElapsedTime)
			return;

		// Updating the location of the character based on the key press
		// providing a beep sound whenver we shift the character
		//2 = character, 3=entrance ,4 = exit ,5 =doors ,6 =keys,7=boss,8 =treasure

		//UP
		if ((g_abKeyPressed[K_UP] || g_abKeyPressed[K_W]) && g_sChar.m_cLocation.Y > 0)
		{
			//Beep(1440, 30);
			ghostplayerY--;
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] != char(1))
			{
				g_sChar.m_cLocation.Y = ghostplayerY;
				bSomethingHappened = true;
			}
			//contact with treasure
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(8))
			{
				mapArray[ghostplayerY][g_sChar.m_cLocation.X] = char(0);
				g_sChar.m_cLocation.Y = ghostplayerY;
				sanity += 30;
				treasureBenefit();
				bSomethingHappened = true;
			}
			//door contact
			if (keys > 0)
			{
				if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(5))
				{
					mapArray[ghostplayerY][g_sChar.m_cLocation.X] = char(0);
					g_sChar.m_cLocation.Y = ghostplayerY;
					bSomethingHappened = true;
					keys -= 1;
				}
			}
			else if (keys <= 0)
			{
				if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(5))
				{

					ghostplayerY++;
					g_sChar.m_cLocation.Y = ghostplayerY;
					bSomethingHappened = true;

				}
			}
			//key contact
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(6))
			{
				keys += 1;
				mapArray[ghostplayerY][g_sChar.m_cLocation.X] = char(0);
				g_sChar.m_cLocation.Y = ghostplayerY;
				bSomethingHappened = true;
			}
			//contact with exit
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(4))
			{
				ExitContact();
				bSomethingHappened = true;
			}
			//entrance contact
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(3))
			{
				EntranceContact();
				bSomethingHappened = true;
			}
			sanityDrop();
			encounterChance();
		}
		//LEFT
		if ((g_abKeyPressed[K_LEFT] || g_abKeyPressed[K_A]) && g_sChar.m_cLocation.X > 0)
		{
			ghostplayerX--;
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] != char(1))
			{
				//Beep(1440, 30);
				g_sChar.m_cLocation.X = ghostplayerX;
				bSomethingHappened = true;
			}
			//contact with treasure
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(8))
			{
				//Beep(1440, 30);
				mapArray[g_sChar.m_cLocation.Y][ghostplayerX] = char(0);
				g_sChar.m_cLocation.X = ghostplayerX;
				treasureBenefit();
				sanity += 30;
				bSomethingHappened = true;
			}
			//door contact
			if (keys > 0)
			{
				if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(5))
				{
					mapArray[g_sChar.m_cLocation.Y][ghostplayerX] = char(0);
					g_sChar.m_cLocation.X = ghostplayerX;
					bSomethingHappened = true;
					keys -= 1;
				}
			}
			else if (keys <= 0)
			{
				if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(5))
				{
					ghostplayerX++;
					g_sChar.m_cLocation.X = ghostplayerX;
					bSomethingHappened = true;

				}
			}

			//key contact
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(6))
			{
				keys += 1;
				mapArray[g_sChar.m_cLocation.Y][ghostplayerX] = char(0);
				g_sChar.m_cLocation.X = ghostplayerX;
				bSomethingHappened = true;
			}
			//contact with exit
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(4))
			{
				ExitContact();
				bSomethingHappened = true;
			}
			//entrance contact
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(3))
			{
				EntranceContact();
				bSomethingHappened = true;
			}
			sanityDrop();
			encounterChance();
		}
		//DOWN
		if ((g_abKeyPressed[K_DOWN] || g_abKeyPressed[K_S]) && g_sChar.m_cLocation.Y < g_Console.getConsoleSize().Y - 1)
		{
			ghostplayerY++;
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] != char(1))
			{
				//Beep(1440, 30);
				g_sChar.m_cLocation.Y = ghostplayerY;
				bSomethingHappened = true;
			}
			//contact with treasure
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(8))
			{
				//Beep(1440, 30);
				mapArray[ghostplayerY][g_sChar.m_cLocation.X] = char(0);
				g_sChar.m_cLocation.Y = ghostplayerY;
				treasureBenefit();
				sanity += 30;
				bSomethingHappened = true;
			}
			//door contact
			if (keys > 0)
			{
				if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(5))
				{
					mapArray[ghostplayerY][g_sChar.m_cLocation.X] = char(0);
					g_sChar.m_cLocation.Y = ghostplayerY;
					bSomethingHappened = true;
					keys -= 1;
				}
			}
			else if (keys <= 0)
			{
				if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(5))
				{
					ghostplayerY--;
					g_sChar.m_cLocation.Y = ghostplayerY;
					bSomethingHappened = true;
				}
			}

			//key contact
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(6))
			{
				keys += 1;
				mapArray[ghostplayerY][g_sChar.m_cLocation.X] = char(0);
				g_sChar.m_cLocation.Y = ghostplayerY;
				bSomethingHappened = true;
			}
			//contact with exit
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(4))
			{
				ExitContact();
				bSomethingHappened = true;
			}
			//entrance contact
			if (mapArray[ghostplayerY][g_sChar.m_cLocation.X] == char(3))
			{
				EntranceContact();
				bSomethingHappened = true;
			}
			sanityDrop();
			encounterChance();
		}
		//RIGHT
		if ((g_abKeyPressed[K_RIGHT] || g_abKeyPressed[K_D]) && g_sChar.m_cLocation.X < g_Console.getConsoleSize().X - 1)
		{
			ghostplayerX++;
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] != char(1))
			{
				//Beep(1440, 30);
				g_sChar.m_cLocation.X = ghostplayerX;
				bSomethingHappened = true;
			}
			//contact with treasure
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(8))
			{
				//Beep(1440, 30);
				mapArray[g_sChar.m_cLocation.Y][ghostplayerX] = char(0);
				g_sChar.m_cLocation.X = ghostplayerX;
				treasureBenefit();
				sanity += 30;
				bSomethingHappened = true;
			}
			//door contact
			if (keys > 0)
			{
				if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(5))
				{
					mapArray[g_sChar.m_cLocation.Y][ghostplayerX] = char(0);
					g_sChar.m_cLocation.X = ghostplayerX;
					bSomethingHappened = true;
					keys -= 1;
				}
			}
			else if (keys <= 0)
			{
				if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(5))
				{
					ghostplayerX--;
					g_sChar.m_cLocation.X = ghostplayerX;
					bSomethingHappened = true;

				}
			}

			//key contact
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(6))
			{
				keys += 1;
				mapArray[g_sChar.m_cLocation.Y][ghostplayerX] = char(0);
				g_sChar.m_cLocation.X = ghostplayerX;
				bSomethingHappened = true;
			}
			//contact with exit
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(4))
			{
				ExitContact();
				bSomethingHappened = true;
			}
			//entrance contact
			if (mapArray[g_sChar.m_cLocation.Y][ghostplayerX] == char(3))
			{
				EntranceContact();
				bSomethingHappened = true;
			}
			sanityDrop();
			encounterChance();
		}
		if (g_abKeyPressed[K_SPACE])
		{
			g_sChar.m_bActive = !g_sChar.m_bActive;
			bSomethingHappened = true;
		}

		if (bSomethingHappened)
		{
			// set the bounce time to some time in the future to prevent accidental triggers
			g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
		}
	}
}

void renderLevels()
{
	COORD c = g_Console.getConsoleSize();

	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	c.Y /= 3;
	c.X = c.X / 2 - 9;
	g_Console.writeToBuffer(c, "LEVEL 1 | Sewers", getColour(option, 0));

	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "LEVEL 2 | Mausoleum", getColour(option, 1));

	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "LEVEL 3 | Horror", getColour(option, 2));

	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "LEVEL 4 | Magic", getColour(option, 3));

	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "LEVEL 5 | Mayhem", getColour(option, 4));


	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "BACK", getColour(option, 5));

	if (g_abKeyPressed[K_DOWN] || g_abKeyPressed[K_S])
	{
		option++;
		if (option > 5)
		{
			option = 0;
		}
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_UP] || g_abKeyPressed[K_W])
	{
		option--;
		if (option < 0)
		{
			option = 5;
		}
		bSomethingHappened = true;
	}

	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
	}
	LevelsControl();
}

void LevelsControl()
{
	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	if (g_abKeyPressed[K_RETURN] && (option == 0))
	{
		maplevel = 1;
		map = 1;
		g_sChar.m_cLocation.X = 104;
		g_sChar.m_cLocation.Y = 20;
		readMapFile();
		g_eGameState = S_STORY;
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_RETURN] && (option == 1))
	{
		maplevel = 2;
		map = 2;
		g_sChar.m_cLocation.X = 2;
		g_sChar.m_cLocation.Y = 1;
		readMapFile();
		g_eGameState = S_STORY;
		bSomethingHappened = true;
	}

	else if (g_abKeyPressed[K_RETURN] && (option == 2))
	{
		maplevel = 3;
		map = 3;
		g_sChar.m_cLocation.X = 4;
		g_sChar.m_cLocation.Y = 4;
		readMapFile();
		g_eGameState = S_STORY;
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_RETURN] && (option == 3))
	{
		maplevel = 4;
		map = 4;
		g_sChar.m_cLocation.X = 3;
		g_sChar.m_cLocation.Y = 2;
		readMapFile();
		g_eGameState = S_STORY;
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_RETURN] && (option == 4))
	{
		maplevel = 5;
		map = 5;
		g_sChar.m_cLocation.X = 57;
		g_sChar.m_cLocation.Y = 2;
		readMapFile();
		g_eGameState = S_STORY;
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_RETURN] && (option == 5))
	{
		g_eGameState = S_MAINMENU;
		bSomethingHappened = true;
	}
	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
	}
}

void renderQuitMenu()
{
	COORD c = g_Console.getConsoleSize();
	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	c.Y /= 3;
	c.X = c.X / 2 - 9;
	g_Console.writeToBuffer(c, "RESUME", getColour(option, 0));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "MAIN MENU", getColour(option, 1));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "QUIT", getColour(option, 2));

	if (g_abKeyPressed[K_DOWN] || g_abKeyPressed[K_S])
	{
		option++;
		if (option > 2)
		{
			option = 0;
		}
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_UP] || g_abKeyPressed[K_W])
	{
		option--;
		if (option < 0)
		{
			option = 2;
		}
		bSomethingHappened = true;
	}

	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
	}
	QuitControls();
}

void QuitControls()
{
	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	if (g_abKeyPressed[K_RETURN] && (option == 0))
	{
		g_eGameState = S_GAME;
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_RETURN] && (option == 1))
	{
		g_eGameState = S_MAINMENU;
		InBattle = false;
		sanity = 200;
		bSomethingHappened = true;
	}

	else if (g_abKeyPressed[K_RETURN] && (option == 2))
	{
		g_bQuitGame = true;
		bSomethingHappened = true;
	}
	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
	}
}

void renderInstructionsMenu()
{
	COORD c = g_Console.getConsoleSize();

	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;


	c.Y /= 3;
	c.X = c.X / 2 - 12;
	g_Console.writeToBuffer(c, "ARROW KEYS OR WASD TO MOVE.", getColour(option, 0));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 16;
	g_Console.writeToBuffer(c, "FIND YOUR WAY THROUGH THE DUNGEON.", getColour(option, 1));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 18;
	g_Console.writeToBuffer(c, "FIND OUT WHY YOU ARE HERE", getColour(option, 2));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 2;
	g_Console.writeToBuffer(c, "BACK-", getColour(option, 3));

	if (g_abKeyPressed[K_DOWN] || g_abKeyPressed[K_S])
	{
		option++;
		if (option > 3)
		{
			option = 0;
		}
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_UP] || g_abKeyPressed[K_W])
	{
		option--;
		if (option < 0)
		{
			option = 3;
		}
		bSomethingHappened = true;
	}

	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
	}
	Instructionscontrols();
}

void Instructionscontrols()
{
	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	if (g_abKeyPressed[K_RETURN] && (option == 3))
	{
		g_eGameState = S_MAINMENU;
		bSomethingHappened = true;
	}
	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
	}
}

void renderMainMenu()
{
	COORD c = g_Console.getConsoleSize();
	enemyNo = 0;

	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;
	ASCIIRead();
	renderASCII();
	c.Y /= 3;
	c.X = c.X / 2 - 9;
	g_Console.writeToBuffer(c, "LEVELS", getColour(option, 0));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "INSTRUCTIONS MENU", getColour(option, 1));
	c.Y += 1;

	c.X = g_Console.getConsoleSize().X / 2 - 9;
	g_Console.writeToBuffer(c, "QUIT", getColour(option, 2));

	if (g_abKeyPressed[K_DOWN] || g_abKeyPressed[K_S])
	{
		option++;
		if (option > 2)
		{
			option = 0;
		}
		bSomethingHappened = true;
	}
	else if (g_abKeyPressed[K_UP] || g_abKeyPressed[K_W])
	{
		option--;
		if (option < 0)
		{
			option = 2;
		}
		bSomethingHappened = true;
	}

	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
	}
	MenuControls();
}

void MenuControls()
{
	COORD c = g_Console.getConsoleSize();

	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;


	if (g_abKeyPressed[K_RETURN] && (option == 0))
	{
		bSomethingHappened = true;
		g_eGameState = S_LEVELS;
	}
	else if (g_abKeyPressed[K_RETURN] && (option == 1))
	{
		bSomethingHappened = true;
		g_eGameState = S_INSTRUCTIONMENU;
	}
	else if (g_abKeyPressed[K_RETURN] && (option == 2))
	{
		bSomethingHappened = true;
		g_bQuitGame = true;
	}

	if (bSomethingHappened)
	{
		// set the bounce time to some time in the future to prevent accidental triggers
		g_dBounceTime = g_dElapsedTime + 0.125; // 125ms should be enough
	}
}

WORD getColour(int active, int current)
{
	if (active == current)
	{
		return 0x0B;
	}
	else
	{
		return 0x07;
	}
}
WORD textColour(int active, int current)
{
	if (active == current)
	{
		return 0x0D;
	}
	else
	{
		return 0x0D;
	}
}
void renderGame()
{
	renderMap();        // renders the map to the buffer first
	renderCharacter();  // renders the character into the buffer
	printSanity();      //Prints sanity counter//
	if (InBattle = true)
	{
		INBATTLE();
	}
}

void renderMap()
{
	//[role][column]
	//2 = character, 3=enterance ,4 = exit ,5 =doors ,6 =keys,7=defence,8 =treasure,9= attack
	//THIS renders the MAP
	//200 ,50
	// - 5+5
	// - 10+10
	for (int y = g_sChar.m_cLocation.Y -5; y < g_sChar.m_cLocation.Y +5; y++)
	{
		for (int x = g_sChar.m_cLocation.X -10; x < g_sChar.m_cLocation.X +10; x++)
		{
	/*for (int y = 0; y < 50; y++)
	{
		for (int x = 0; x < 200; x++)
		{*/
			if (mapArray[y][x] == 1)
			{
				g_Console.writeToBuffer(x, y, ' ', 0x40);
			}
			else if (mapArray[y][x] == 0)
			{
				g_Console.writeToBuffer(x, y, ' ', 0x07);
			}
			else if (mapArray[y][x] == 2)
			{
				g_Console.writeToBuffer(x, y, 'C');
			}
			else if (mapArray[y][x] == 3)
			{
				g_Console.writeToBuffer(x, y, 'S');
			}
			else if (mapArray[y][x] == 4)
			{
				g_Console.writeToBuffer(x, y, 'E');
			}
			else if (mapArray[y][x] == 5)
			{
				g_Console.writeToBuffer(x, y, 'D');
			}
			else if (mapArray[y][x] == 6)
			{
				g_Console.writeToBuffer(x, y, 'K');
			}
			else if (mapArray[y][x] == 8)
			{
				g_Console.writeToBuffer(x, y, 'T');
			}
		}
	}

}

void renderstory()
{
	COORD c = g_Console.getConsoleSize();

	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;
	if (map == 1)
	{
		c.Y /= 3;
		c.X = c.X / 2 - 18;
		g_Console.writeToBuffer(c, "It's dark. Your eyes slowly adjust to the darkness.", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 18;
		g_Console.writeToBuffer(c, "You see the walls. And another wall. Where do you go?", textColour(option, 0));
		c.Y += 1;

		c.X = g_Console.getConsoleSize().X / 2 - 18;
		g_Console.writeToBuffer(c, "One thing�s for certain, there's a way out.", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 18;
		g_Console.writeToBuffer(c, "PRESS ENTER TO CONTINUE", textColour(option, 0));

	}
	if (map == 2)
	{
		c.Y /= 3;
		c.X = 20;
		g_Console.writeToBuffer(c, "The noises you heard as you slowly made your way through the maze seemed otherworldly, unnatural, even. ", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 35;
		g_Console.writeToBuffer(c, "Maybe they were all just a part of your mind, but how else are you to find out?", textColour(option, 0));
		c.Y += 1;

		c.X = g_Console.getConsoleSize().X / 2 - 26;
		g_Console.writeToBuffer(c, "The only way you can go is down now, maybe you�ll find answers there.", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 18;
		g_Console.writeToBuffer(c, "PRESS ENTER TO CONTINUE", textColour(option, 0));
	}
	if (map == 3)
	{
		c.Y /= 3;
		c.X = c.X / 2 - 30;
		g_Console.writeToBuffer(c, "he further you go, the more doubts gripe at you. You aren't alone.", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 30;
		g_Console.writeToBuffer(c, "You CAN�T be. There has to be something along with you in here.", textColour(option, 0));
		c.Y += 1;

		c.X = g_Console.getConsoleSize().X / 2 - 34;
		g_Console.writeToBuffer(c, "You feel its presence as you creep your way through the maze, but is it really there?", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 18;
		g_Console.writeToBuffer(c, "PRESS ENTER TO CONTINUE", textColour(option, 0));
	}
	if (map == 4)
	{
		c.Y /= 3;
		c.X = c.X / 2 - 35;
		g_Console.writeToBuffer(c, "You saw it this time. Right as you found the exit for the floor, it had appeared.", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 44;
		g_Console.writeToBuffer(c, "And just as quickly, it ran. It was too fast for you to make out what it was, just a large blur.", textColour(option, 0));
		c.Y += 1;

		c.X = g_Console.getConsoleSize().X / 2 - 30;
		g_Console.writeToBuffer(c, "More noises, almost overpowering your own thoughts. No time to think, just go down.", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 15;
		g_Console.writeToBuffer(c, "PRESS ENTER TO CONTINUE", textColour(option, 0));
	}
	if (map == 5)
	{
		c.Y /= 3;
		c.X = c.X / 2 - 50;
		g_Console.writeToBuffer(c, "Maybe you are going insane. But you can�t tell. An insane person can�t tell that they�re off the deep end right?", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 34;
		g_Console.writeToBuffer(c, "No matter, you don�t know why, but you can just feel it. It's about to end.", textColour(option, 0));
		c.Y += 1;

		c.X = g_Console.getConsoleSize().X / 2 - 24;
		g_Console.writeToBuffer(c, "Trudge deeper you tell yourself, it will end soon.", textColour(option, 0));
		c.Y += 1;
		c.X = g_Console.getConsoleSize().X / 2 - 18;
		g_Console.writeToBuffer(c, "PRESS ENTER TO CONTINUE", textColour(option, 0));
	}
	StoryControls();
}

void StoryControls()
{
	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	if (g_abKeyPressed[K_RETURN])
	{
		g_eGameState = S_GAME;
		bSomethingHappened = true;
	}

}


void renderCharacter()
{
	// Draw the location of the character
	WORD charColor = 0x0C;
	if (g_sChar.m_bActive)
	{
		charColor = 0x0B;
	}
	g_Console.writeToBuffer(g_sChar.m_cLocation, "C", charColor);
}

void renderFramerate()
{
	COORD c;
	// displays the framerate
	std::ostringstream ss;
	ss << std::fixed << std::setprecision(3);
	ss << 1.0 / g_dDeltaTime << "fps";
	c.X = g_Console.getConsoleSize().X - 11;
	c.Y = 37;
	g_Console.writeToBuffer(c, ss.str());

	// displays the elapsed time
	ss.str("");
	ss << g_dElapsedTime << "secs";
	c.X = g_Console.getConsoleSize().X - 11;
	c.Y = 38;
	g_Console.writeToBuffer(c, ss.str(), 0x0F);
}

void renderToScreen()
{
	// Writes the buffer to the console, hence you will see what you have written
	g_Console.flushBufferToConsole();
}

void readMapFile()
{

	string data = "";
	string path = GetDiretoryInfo() + "\\Map" + to_string(maplevel) + ".txt";
	ifstream file(path);

	if (maplevel == 1)
	{
		for (int y = 0; y < 33; y++)
		{
			for (int x = 0; x < 121; x++)
			{
				getline(file, data, ',');
				mapArray[y][x] = stoi(data);


			}

		}
	}
	else if (maplevel == 2)
	{
		for (int y = 0; y < 33; y++)
		{
			for (int x = 0; x < 121; x++)
			{
				getline(file, data, ',');
				mapArray[y][x] = stoi(data);

			}

		}
	}
	else if (maplevel == 3)
	{
		for (int y = 0; y < 33; y++)
		{
			for (int x = 0; x < 121; x++)
			{
				getline(file, data, ',');
				mapArray[y][x] = stoi(data);
			}

		}
	}
	else if (maplevel == 4)
	{
		for (int y = 0; y < 33; y++)
		{
			for (int x = 0; x < 121; x++)
			{
				getline(file, data, ',');
				mapArray[y][x] = stoi(data);
			}

		}
	}
	else if (maplevel == 5)
	{
		for (int y = 0; y < 33; y++)
		{
			for (int x = 0; x < 121; x++)
			{
				getline(file, data, ',');
				mapArray[y][x] = stoi(data);
			}

		}
	}

	file.close();
}

string GetDiretoryInfo()
{
	WCHAR directorychar[MAX_PATH]; //Initialize directorychar with 260 char limit
	wstring(directorychar, GetModuleFileNameW(NULL, directorychar, MAX_PATH)); //Get Directory Location

	using convert_type = codecvt_utf8<wchar_t>; //Declare Convertion of wchar to unicode utf_8
	wstring_convert<convert_type, wchar_t> converter; //Intialize converter
	string directory = converter.to_bytes(directorychar); //convert directory location from wstring to string

	directory = directory.substr(0, directory.find_last_of("\\")); //Remove Executable name and .exe from the string path

	return directory; //Return Game File path
}

//Handles Going To Previous Level//
void EntranceContact()
{
	if (maplevel == 1)
	{
		g_eGameState = S_MAINMENU;

	}
	else if (maplevel == 2)
	{
		maplevel = 1;
		readMapFile();
		g_eGameState = S_GAME;

	}
	else if (maplevel == 3)
	{
		maplevel = 2;
		readMapFile();
		g_eGameState = S_GAME;

	}
	else if (maplevel == 4)
	{
		maplevel = 3;
		readMapFile();
		g_eGameState = S_GAME;

	}
	else if (maplevel == 5)
	{
		maplevel = 4;
		readMapFile();
		g_eGameState = S_GAME;

	}
}

//Handles Going To Next Level//
void ExitContact()
{
	if (maplevel == 1)
	{
		maplevel = 2;
		readMapFile();
		g_eGameState = S_GAME;
		g_sChar.m_cLocation.X = 2;
		g_sChar.m_cLocation.Y = 1;
	}
	else if (maplevel == 2)
	{
		maplevel = 3;
		readMapFile();
		g_eGameState = S_STORY;
		g_sChar.m_cLocation.X = 4;
		g_sChar.m_cLocation.Y = 4;
	}
	else if (maplevel == 3)
	{
		maplevel = 4;
		readMapFile();
		g_eGameState = S_STORY;
		g_sChar.m_cLocation.X = 3;
		g_sChar.m_cLocation.Y = 2;
	}
	else if (maplevel == 4)
	{
		maplevel = 5;
		readMapFile();
		g_eGameState = S_STORY;
		g_sChar.m_cLocation.X = 57;
		g_sChar.m_cLocation.Y = 2;
	}
	else if (maplevel = 5)
	{
		g_eGameState = S_WIN;
	}
}

void ASCIIRead()
{
	string key = "";
	string path = GetDiretoryInfo() + "\\ASCII" + to_string(enemyNo) + ".txt";
	ifstream file(path);

	if (g_eGameState == S_MAINMENU || g_eGameState == S_LOADING)
	{
		for (int y = 0; y < 12; y++)
		{
			for (int x = 0; x < 95; x++)
			{
				getline(file, key, ',');
				titleArray[y][x] = stoi(key);

			}

		}
	}
	else
	{
		for (int y = 0; y < 40; y++)
		{
			for (int x = 0; x < 40; x++)
			{
				getline(file, key, ',');
				artArray[y][x] = stoi(key);

			}

		}
	}
}

void renderASCII()
{

	//|= 1, '=2, _ = 3, (= 4, ) = 5, /= 6, \ = 7, ,=8,`=9
	if (g_eGameState == S_MAINMENU || g_eGameState == S_LOADING)
	{

		for (int y = 0; y < 12; y++)
		{
			for (int x = 0; x < 95; x++)
			{
				if (titleArray[y][x] == 0)
				{
					g_Console.writeToBuffer(x, y, '|');
				}
				else if (titleArray[y][x] == 1)
				{
					g_Console.writeToBuffer(x, y, 'v');
				}
				else if (titleArray[y][x] == 2)
				{
					g_Console.writeToBuffer(x, y, '_');
				}
				else if (titleArray[y][x] == 3)
				{
					g_Console.writeToBuffer(x, y, '(');
				}
				else if (titleArray[y][x] == 4)
				{
					g_Console.writeToBuffer(x, y, ')');
				}
				else if (titleArray[y][x] == 5)
				{
					g_Console.writeToBuffer(x, y, '/');
				}
				else if (titleArray[y][x] == 6)
				{
					g_Console.writeToBuffer(x, y, '\\');
				}
				else if (titleArray[y][x] == 7)
				{
					g_Console.writeToBuffer(x, y, ' ');
				}
				else if (titleArray[y][x] == 8)
				{
					g_Console.writeToBuffer(x, y, ',');
				}
				else if (titleArray[y][x] == 9)
				{
					g_Console.writeToBuffer(x, y, '`');
				}
			}
		}
	}
	else
	{
		for (int y = 0; y < 40; y++)
		{
			for (int x = 0; x < 40; x++)
			{
				if (artArray[y][x] == 0)//black
				{
					g_Console.writeToBuffer(x, y, ' ', 0x00);
				}
				else if (artArray[y][x] == 1)//white
				{
					g_Console.writeToBuffer(x, y, ' ', 0x77);
				}
				else if (artArray[y][x] == 2)//blue
				{
					g_Console.writeToBuffer(x, y, ' ', 0x11);
				}
				else if (artArray[y][x] == 3)//green
				{
					g_Console.writeToBuffer(x, y, ' ', 0x22);
				}
				else if (artArray[y][x] == 4)//aqua
				{
					g_Console.writeToBuffer(x, y, ' ', 0x33);
				}
				else if (artArray[y][x] == 5)//red
				{
					g_Console.writeToBuffer(x, y, ' ', 0x44);
				}
				else if (artArray[y][x] == 6)//purple
				{
					g_Console.writeToBuffer(x, y, ' ', 0x55);
				}
				else if (artArray[y][x] == 7)//yellow
				{
					g_Console.writeToBuffer(x, y, ' ', 0x66);
				}
				else if (artArray[y][x] == 8)//grey
				{
					g_Console.writeToBuffer(x, y, ' ', 0x88);
				}
			}

		}
	}
}

void printSanity()
{
	COORD c = g_Console.getConsoleSize();
	c.X = 2;
	c.Y = 35;
	string sanityMeter = "Sanity | " + to_string(sanity);
	g_Console.writeToBuffer(c, sanityMeter, 0x0A);

	c.X = 17;
	c.Y = 35;
	string keyPrint = "Keys | " + to_string(keys);
	g_Console.writeToBuffer(c, keyPrint, 0x0A);

	c.X = 32;
	c.Y = 35;
	string HPno = "Health |" + to_string(user.health);
	g_Console.writeToBuffer(c, HPno, 0x0A);

	c.X = 47;
	c.Y = 35;
	string ATKno = "Attack |" + to_string(user.attack);
	g_Console.writeToBuffer(c, ATKno, 0x0A);

	c.X = 62;
	c.Y = 35;
	string DEFno = "Defence |" + to_string(user.defence);
	g_Console.writeToBuffer(c, DEFno, 0x0A);

	c.X = 77;
	c.Y = 35;
	string POTno = "Potions |" + to_string(user.potions);
	g_Console.writeToBuffer(c, POTno, 0x0A);

	c.X = 92;
	c.Y = 35;
	string EXPno = "Experience |" + to_string(user.exp);
	g_Console.writeToBuffer(c, EXPno, 0x0A);

	c.X = 115;
	c.Y = 35;
	string LVLno = "Level |" + to_string(user.level);
	g_Console.writeToBuffer(c, LVLno, 0x0A);

	c.X = 2;
	c.Y = 36;
	g_Console.writeToBuffer(c, monName, 0x0C);

	c.X = 17;
	c.Y = 36;
	string monHP = "Health |" + to_string(enemyMon.monhealth);
	g_Console.writeToBuffer(c, monHP, 0x0C);
	
	c.X = 32;
	c.Y = 36;
	string monATK = "Attack |" + to_string(enemyMon.monattack);
	g_Console.writeToBuffer(c, monATK, 0x0C);

	c.X = 47;
	c.Y = 36;
	string monDEF = "Defence |" + to_string(enemyMon.mondefence);
	g_Console.writeToBuffer(c, monDEF, 0x0C);
}

void sanityDrop()
{
	int sanityDrop;
	sanityDrop = (rand() % 7) + 1;
	if (sanityDrop == 1)
	{
		sanity--;
	}
	if (sanity == 0)
	{
		g_eGameState = S_DEATH;
	}
}

void renderplayerDied()
{
	COORD c = g_Console.getConsoleSize();

	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	c.Y = 20;
	c.X = 30;
	g_Console.writeToBuffer(c, "YOU HAVE DIED ", textColour(option, 0));

	c.Y = 24;
	c.X = 30;
	g_Console.writeToBuffer(c, "GAME OVER  ", textColour(option, 0));

	c.Y = 28;
	c.X = 30;
	g_Console.writeToBuffer(c, "PRESS ENTER TO CONTINUE", textColour(option, 0));

	DeathControls();
}

void DeathControls()
{
	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	if (g_abKeyPressed[K_RETURN])
	{
		g_eGameState = S_MAINMENU;
		bSomethingHappened = true;
	}
}
void WinScreen()
{

	COORD c = g_Console.getConsoleSize();

	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	c.Y = 3;
	c.X = c.X / 2 - 30;
	g_Console.writeToBuffer(c, "Run faster. They can�t get you that way. You didn't do anything wrong.", textColour(option, 0));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 35;
	g_Console.writeToBuffer(c, "You were stuck in that maze for so long, you couldn't have been the one to kill them.", textColour(option, 0));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 30;
	g_Console.writeToBuffer(c, " The feeling of that �thing� chasing you through the maze washes over you again.", textColour(option, 0));
	c.Y += 1;
	c.X = g_Console.getConsoleSize().X / 2 - 18;
	g_Console.writeToBuffer(c, "Keep running, off the deep end you go.", textColour(option, 0));
	
	c.Y = 24;
	c.X = 30;
	g_Console.writeToBuffer(c, "LEVI IS OUT", getColour(option, 0));

	c.Y = 26;
	c.X = 30;
	g_Console.writeToBuffer(c, "GAME FINISH ", getColour(option, 0));

	c.Y = 28;
	c.X = 30;
	g_Console.writeToBuffer(c, "PRESS ENTER TO GO BACK TO MAIN MENU", getColour(option, 0));

	WinControls();
}

void WinControls()
{
	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
		return;

	if (g_abKeyPressed[K_RETURN])
	{
		g_eGameState = S_MAINMENU;
		bSomethingHappened = true;
	}
}
void ambientMusic(void)
{
	PlaySound(TEXT("ambientMusic.wav"), GetModuleHandle(NULL), SND_FILENAME | SND_ASYNC | SND_LOOP);
}

void levelup(void)
{
	user.attack += 1;
	user.defence += 1;
	user.level += 1;
	user.exp = 0;
}
void HealUP(void)
{
	COORD c = g_Console.getConsoleSize();
	if (user.potions>0)
	{
		user.health += 50;
		sanity += 10;
		user.potions -= 1;
	}
	else
	{
		c.X = 5;
		c.Y = 35;
		g_Console.writeToBuffer(c, "you have no potions", 0x47);
	}
}
void treasureBenefit(void)
{
	int reward = rand() % 4 + 1;
	reward = rand() % 4 + 1;
	switch (reward)
	{
	case 1:user.attack += 1;
		break;
	case 2:user.defence += 1;
		break;
	case 3:user.exp += 50;
		break;
	case 4:user.potions += 5;
		break;
	}
}

void enemyRand()
{
	
	monsterID mob;
	if (maplevel == 1)
	{
		mob = (monsterID)(rand() % 3 );//0-2
		mob = (monsterID)(rand() % 3 );
	}
	else if (maplevel == 2)
	{
		mob = (monsterID)(rand() % 5 + 3);//3-5
		mob = (monsterID)(rand() % 5 + 3);
	}
	else if (maplevel == 3)
	{
		mob = (monsterID)(rand() % 8 + 6);//6-8
		mob = (monsterID)(rand() % 8 + 6);
	}
	else if (maplevel == 4)
	{
		mob = (monsterID)(rand() % 11 + 9);//9-11
		mob = (monsterID)(rand() % 11 + 9);
	}
	else if (maplevel == 5)
	{
		mob = (monsterID)(rand() % 14 + 12);//12-14
		mob = (monsterID)(rand() % 14 + 12);
	}
	switch (mob)
	{
	case M_Slime:
		monName = "Slime";
		enemyMon.monhealth = enemyStatsArray[0][0];
		enemyMon.monattack = enemyStatsArray[1][0];
		enemyMon.mondefence = enemyStatsArray[2][0];
		break;
	case M_GiantRat:
		monName = "GiantRat";
		enemyMon.monhealth = enemyStatsArray[0][1];
		enemyMon.monattack = enemyStatsArray[1][1];
		enemyMon.mondefence = enemyStatsArray[2][1];
		break;
	case M_Bats:
		monName = "Bats";
		enemyMon.monhealth = enemyStatsArray[0][2];
		enemyMon.monattack = enemyStatsArray[1][2];
		enemyMon.mondefence = enemyStatsArray[2][2];
		break;
	case M_Skeleton:
		monName = "Skeleton";
		enemyMon.monhealth = enemyStatsArray[0][3];
		enemyMon.monattack = enemyStatsArray[1][3];
		enemyMon.mondefence = enemyStatsArray[2][3];
		break;
	case M_Zombie:
		monName = "Zombies";
		enemyMon.monhealth = enemyStatsArray[0][4];
		enemyMon.monattack = enemyStatsArray[1][4];
		enemyMon.mondefence = enemyStatsArray[2][4];
		break;
	case M_undeadHounds:
		monName = "UndeadHounds";
		enemyMon.monhealth = enemyStatsArray[0][5];
		enemyMon.monattack = enemyStatsArray[1][5];
		enemyMon.mondefence = enemyStatsArray[2][5];
		break;
	case M_Vampire:
		monName = "Vampire";
		enemyMon.monhealth = enemyStatsArray[0][6];
		enemyMon.monattack = enemyStatsArray[1][6];
		enemyMon.mondefence = enemyStatsArray[2][6];
		break;
	case M_Frankenstein:
		monName = "Frankenstein";
		enemyMon.monhealth = enemyStatsArray[0][7];
		enemyMon.monattack = enemyStatsArray[1][7];
		enemyMon.mondefence = enemyStatsArray[2][7];
		break;
	case M_Mummy:
		monName = "Mummy";
		enemyMon.monhealth = enemyStatsArray[0][8];
		enemyMon.monattack = enemyStatsArray[1][8];
		enemyMon.mondefence = enemyStatsArray[2][8];
		break;
	case M_Necromancer:
		monName = "Necromancer";
		enemyMon.monhealth = enemyStatsArray[0][9];
		enemyMon.monattack = enemyStatsArray[1][9];
		enemyMon.mondefence = enemyStatsArray[2][9];
		break;
	case M_Witch:
		monName = "Witch";
		enemyMon.monhealth = enemyStatsArray[0][10];
		enemyMon.monattack = enemyStatsArray[1][10];
		enemyMon.mondefence = enemyStatsArray[2][10];
		break;
	case M_Occultist:
		monName = "Occultist";
		enemyMon.monhealth = enemyStatsArray[0][11];
		enemyMon.monattack = enemyStatsArray[1][11];
		enemyMon.mondefence = enemyStatsArray[2][11];
		break;
	case M_Unicorn:
		monName = "Unicorn";
		enemyMon.monhealth = enemyStatsArray[0][12];
		enemyMon.monattack = enemyStatsArray[1][12];
		enemyMon.mondefence = enemyStatsArray[2][12];
		break;
	case M_GiantAnts:
		monName = "GiantAnts";
		enemyMon.monhealth = enemyStatsArray[0][13];
		enemyMon.monattack = enemyStatsArray[1][13];
		enemyMon.mondefence = enemyStatsArray[2][13];
		break;
	case M_Centaur:
		monName = "Centaur";
		enemyMon.monhealth = enemyStatsArray[0][14];
		enemyMon.monattack = enemyStatsArray[1][14];
		enemyMon.mondefence = enemyStatsArray[2][14];
		break;
	default:
		monName = "Slime";
		enemyMon.monhealth = enemyStatsArray[0][0];
		enemyMon.monattack = enemyStatsArray[1][0];
		enemyMon.mondefence = enemyStatsArray[2][0];
		break;
	}
}

void encounterChance()
{
	
	int chance = rand() % 10 + 1;
	if (chance == 1)
	{
		InBattle = true;
		enemyRand();
		monsterAlive = true;
		
	}
}
void INBATTLE(void)
{

	bool bSomethingHappened = false;
	if (g_dBounceTime > g_dElapsedTime)
	{
		return;
	}
	int tempdmg;
	PlayerTurn = true;
	string outputlog;
	COORD c = g_Console.getConsoleSize();

	if (g_abKeyPressed[K_1]&& (PlayerTurn==true))
		{
			tempdmg = (user.attack - enemyMon.mondefence);
			if (tempdmg <= 0)
			{
				enemyMon.monhealth -= 1;
				outputlog = "you dealt 1 damage due to the opponent's defence ";
				
			}
			else 
			{
				enemyMon.monhealth -= tempdmg;
				outputlog = "you dealt "+ to_string(tempdmg) +" damage to the opponent";
				
			}
			if (enemyMon.monhealth>0)
			{
				PlayerTurn = false;
			}
			bSomethingHappened = true;
		}
		if (g_abKeyPressed[K_2] && (PlayerTurn == true))
		{			
			HealUP();
			if (enemyMon.monhealth>0)
			{
				PlayerTurn = false;
			}bSomethingHappened = true;
		}
		if (g_abKeyPressed[K_3] && (PlayerTurn == true))
		{
			int runChance = rand() % 4 + 1;
			switch (runChance)
			{
			case 1:
				user.health -= enemyMon.monattack;
				sanity -= 15;
				PlayerTurn = false;
				break;
			case 2:
				sanity -= 5;
				PlayerTurn = false;
				break;
			case 3:
				user.health -= enemyMon.monattack;
				InBattle = false;
				PlayerTurn = true;
				break;
			case 4:
				InBattle = false;
				PlayerTurn = true;
				break;
			default: InBattle = false;
				PlayerTurn = true;
				break;
			}
			bSomethingHappened = true;
		}
		if (bSomethingHappened)
		{
			// set the bounce time to some time in the future to prevent accidental triggers
			g_dBounceTime = g_dElapsedTime + 0.140; // 125ms should be enough
		}
		if (PlayerTurn == false)
		{
			tempdmg = (enemyMon.monattack - user.defence);
			if (tempdmg <= 0)
			{
				user.health -= 1;
			}
			else
			{
				user.health -= tempdmg;
			}
			
		}
	
	if (enemyMon.monhealth < 0)
	{
<<<<<<< HEAD
		PlayerTurn = true;
		if (enemyMon.monhealth == 1)
		{
=======
		
>>>>>>> 07c3f55dbdc122d6b568a3803f9a3582e5fa0ce7
			switch (maplevel)
			{
			case 1:
				user.exp += 10;
				user.potions += 1;
				break;
			case 2:
				user.exp += 20;
				user.potions += 1;
				break;
			case 3:
				user.exp += 30;
				user.potions += 2;
				break;
			case 4:
				user.exp += 40;
				user.potions += 2;
				break;
			case 5:
				user.exp += 50;
				user.potions += 3;
				break;

			default:
				user.exp += 50;
				user.potions += 3;
				break;
			
			enemyMon.monhealth = 0;
			monsterAlive = false;
		}
		
		InBattle = false;
	}
	c.X = 5;
	c.Y = 38;
	g_Console.writeToBuffer(c, outputlog, 0x0E);
	c.X = 5;
	c.Y = 40;
	g_Console.writeToBuffer(c, "(Key1)ATTACK", 0x0B);
	c.X = 5;
	c.Y = 41;
	g_Console.writeToBuffer(c, "(Key2)Heal", 0x0B);
	c.X = 5;
	c.Y = 42;
	g_Console.writeToBuffer(c, "(Key3)RUN", 0x0B);

	
}
