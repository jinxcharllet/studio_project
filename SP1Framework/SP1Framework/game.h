#ifndef _GAME_H
#define _GAME_H

#include "Framework\timer.h"


extern CStopWatch g_swTimer;
extern bool g_bQuitGame;

// Enumeration to store the control keys that your game will have
enum EKEYS
{
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
	K_W,
	K_S,
	K_A,
	K_D,
	K_1,
	K_2,
	K_3,
	K_4,
	K_ESCAPE,
    K_SPACE,
	K_COUNT,
    K_RETURN
};

// Enumeration for the different screen states
enum EGAMESTATES
{
	S_QUIT,
	S_LOADING,
	S_MAINMENU,
	S_SPLASHSCREEN,
	S_GAME,
	S_STORY,
	S_DEATH,
	S_INSTRUCTIONMENU,
	S_QUITMENU,
	S_BATTLE,
	S_LEVELS,
	S_WIN,
	S_COUNT
};
enum monsterID
{
	M_Slime,
	M_GiantRat,
	M_Bats,
	M_Skeleton,
	M_Zombie,
	M_undeadHounds,
	M_Vampire,
	M_Frankenstein,
	M_Mummy,
	M_Necromancer,
	M_Witch,
	M_Occultist,
	M_Unicorn,
	M_GiantAnts,
	M_Centaur

};
enum monsterEvent
{
	MONSTERATT = 1,
	MONSTERBLOCK
};

// struct for the game character
struct SGameChar
{
    COORD m_cLocation;
    bool  m_bActive;
};
struct playerStats
{
	int health;
	int attack;
	int defence;
	int exp;
	int level;
	int potions;
};
struct monsterStats
{
	
	int monhealth;
	int monattack;
	int mondefence;

};

void init        ( void );      // initialize your variables, allocate memory, etc
void getInput    ( void );      // get input from player
void update      ( double dt ); // update the game and the state of the game
void render      ( void );      // renders the current state of the game to the console
void shutdown    ( void );      // do clean up, free memory

void LOADING();    // waits for time to pass in splash screen [d]
void gameplay();            // gameplay logic
void moveCharacter();       // moves the character, collision detection, physics, etc
void processUserInput();    // checks if you should change states or do something else with the game, e.g. pause, exit
void clearScreen();         // clears the current screen and draw from scratch 
//void renderSplashScreen();  // renders the splash screen[d]
void renderGame();          // renders the game stuff
void renderMap();           // renders the map to the buffer first
void renderCharacter();     // renders the character into the buffer
void renderFramerate();     // renders debug information, frame rate, elapsed time, etc
void renderToScreen();      // dump the contents of the buffer to the screen, one frame worth of game
void renderMainMenu();      //
void renderInstructionsMenu();
void renderQuitMenu();
void QuitControls();
void MenuControls();
void Instructionscontrols();
void renderLevels();
void renderLOADING();
void LevelsControl();
void readMapFile();
void EntranceContact();
void ExitContact();
void renderstory();
void StoryControls();
void printSanity();          //Prints sanity and key amount//
void sanityDrop();           //Handles Dropping Sanity//
void renderplayerDied();
void DeathControls();
void ambientMusic();
void WinControls();
void WinScreen();

WORD getColour(int active, int current);
WORD textColour(int active, int current);

void ASCIIRead();
void renderASCII();
void levelup();
void HealUP();
void treasureBenefit();
void enemyRand();
void encounterChance();
void INBATTLE();

#endif // _GAME_H